/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/page";
exports.ids = ["pages/page"];
exports.modules = {

/***/ "./pages/page.js":
/*!***********************!*\
  !*** ./pages/page.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStaticProps\": function() { return /* binding */ getStaticProps; }\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n\n\nvar _jsxFileName = \"/home/rds/Projects/elemetor-api/pages/page.js\";\n\nconst components = {};\n\nconst Page = props => {\n  const listItems = props.dados.map(row => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    className: \"row\",\n    children: row.elements.map(column => {\n      return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: `col-${column.settings._column_size}`,\n        children: column.elements.map(widget => {\n          if (ucwords(widget.widgetType) === 'Image') {\n            return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"img\", {\n              src: widget.settings.image.url,\n              alt: widget.settings.image.alt\n            }, widget.id, false, {\n              fileName: _jsxFileName,\n              lineNumber: 18,\n              columnNumber: 21\n            }, undefined);\n          } else if (ucwords(widget.widgetType) === 'Heading') {\n            return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"heading\", {\n              className: \"\",\n              children: widget.settings.title\n            }, widget.id, false, {\n              fileName: _jsxFileName,\n              lineNumber: 22,\n              columnNumber: 21\n            }, undefined);\n          }\n        })\n      }, column.id, false, {\n        fileName: _jsxFileName,\n        lineNumber: 13,\n        columnNumber: 13\n      }, undefined);\n    })\n  }, row.id.toString(), false, {\n    fileName: _jsxFileName,\n    lineNumber: 9,\n    columnNumber: 5\n  }, undefined));\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n    children: listItems\n  }, void 0, false);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Page);\nconst getStaticProps = async () => {\n  let data;\n  await fetch('http://localhost/?rest_route=/wp/v2/pages/pg-contact').then(function (res) {\n    return res.json();\n  }).then(function (json) {\n    data = json;\n    return data;\n  });\n  return {\n    props: {\n      dados: data // will be passed to the page component as props\n\n    }\n  };\n};\n\nfunction ucwords(text) {\n  let str = text.toLowerCase();\n  return str.replace(/(^([a-zA-Z\\p{M}]))|([ -][a-zA-Z\\p{M}])/g, function (s) {\n    return s.toUpperCase();\n  });\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teS1hcHAvLi9wYWdlcy9wYWdlLmpzPzkzZGMiXSwibmFtZXMiOlsiY29tcG9uZW50cyIsIlBhZ2UiLCJwcm9wcyIsImxpc3RJdGVtcyIsImRhZG9zIiwibWFwIiwicm93IiwiZWxlbWVudHMiLCJjb2x1bW4iLCJzZXR0aW5ncyIsIl9jb2x1bW5fc2l6ZSIsIndpZGdldCIsInVjd29yZHMiLCJ3aWRnZXRUeXBlIiwiaW1hZ2UiLCJ1cmwiLCJhbHQiLCJpZCIsInRpdGxlIiwidG9TdHJpbmciLCJnZXRTdGF0aWNQcm9wcyIsImRhdGEiLCJmZXRjaCIsInRoZW4iLCJyZXMiLCJqc29uIiwidGV4dCIsInN0ciIsInRvTG93ZXJDYXNlIiwicmVwbGFjZSIsInMiLCJ0b1VwcGVyQ2FzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTtBQUVBLE1BQU1BLFVBQVUsR0FBRyxFQUFuQjs7QUFHQSxNQUFNQyxJQUFJLEdBQUdDLEtBQUssSUFBSTtBQUVwQixRQUFNQyxTQUFTLEdBQUdELEtBQUssQ0FBQ0UsS0FBTixDQUFZQyxHQUFaLENBQWlCQyxHQUFELGlCQUNoQztBQUE2QixhQUFTLEVBQUMsS0FBdkM7QUFBQSxjQUVJQSxHQUFHLENBQUNDLFFBQUosQ0FBYUYsR0FBYixDQUFpQkcsTUFBTSxJQUFJO0FBQ3pCLDBCQUNFO0FBQXFCLGlCQUFTLEVBQUcsT0FBTUEsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxZQUFhLEVBQXBFO0FBQUEsa0JBRUVGLE1BQU0sQ0FBQ0QsUUFBUCxDQUFnQkYsR0FBaEIsQ0FBb0JNLE1BQU0sSUFBSTtBQUM1QixjQUFHQyxPQUFPLENBQUNELE1BQU0sQ0FBQ0UsVUFBUixDQUFQLEtBQStCLE9BQWxDLEVBQTJDO0FBQ3pDLGdDQUNFO0FBQUssaUJBQUcsRUFBRUYsTUFBTSxDQUFDRixRQUFQLENBQWdCSyxLQUFoQixDQUFzQkMsR0FBaEM7QUFBcUMsaUJBQUcsRUFBRUosTUFBTSxDQUFDRixRQUFQLENBQWdCSyxLQUFoQixDQUFzQkU7QUFBaEUsZUFBMEVMLE1BQU0sQ0FBQ00sRUFBakY7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERjtBQUdELFdBSkQsTUFJTSxJQUFHTCxPQUFPLENBQUNELE1BQU0sQ0FBQ0UsVUFBUixDQUFQLEtBQStCLFNBQWxDLEVBQTZDO0FBQ2pELGdDQUNFO0FBQXlCLHVCQUFTLEVBQUMsRUFBbkM7QUFBQSx3QkFBdUNGLE1BQU0sQ0FBQ0YsUUFBUCxDQUFnQlM7QUFBdkQsZUFBY1AsTUFBTSxDQUFDTSxFQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQURGO0FBR0Q7QUFDRixTQVZEO0FBRkYsU0FBVVQsTUFBTSxDQUFDUyxFQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGO0FBaUJELEtBbEJEO0FBRkosS0FBVVgsR0FBRyxDQUFDVyxFQUFKLENBQU9FLFFBQVAsRUFBVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRGdCLENBQWxCO0FBMkJBLHNCQUNFO0FBQUEsY0FDRWhCO0FBREYsbUJBREY7QUFLRCxDQWxDRDs7QUFxQ0EsK0RBQWVGLElBQWY7QUFFTyxNQUFNbUIsY0FBYyxHQUFHLFlBQVk7QUFFeEMsTUFBSUMsSUFBSjtBQUNBLFFBQU1DLEtBQUssQ0FBQyxzREFBRCxDQUFMLENBQ0hDLElBREcsQ0FDRSxVQUFVQyxHQUFWLEVBQWU7QUFDbkIsV0FBT0EsR0FBRyxDQUFDQyxJQUFKLEVBQVA7QUFDRCxHQUhHLEVBR0RGLElBSEMsQ0FHSSxVQUFVRSxJQUFWLEVBQWdCO0FBQ3RCSixRQUFJLEdBQUdJLElBQVA7QUFDQSxXQUFPSixJQUFQO0FBQ0QsR0FORyxDQUFOO0FBUUEsU0FBTztBQUNMbkIsU0FBSyxFQUFFO0FBQ0xFLFdBQUssRUFBRWlCLElBREYsQ0FDUTs7QUFEUjtBQURGLEdBQVA7QUFLRCxDQWhCTTs7QUFrQlAsU0FBU1QsT0FBVCxDQUFpQmMsSUFBakIsRUFBdUI7QUFFckIsTUFBSUMsR0FBRyxHQUFHRCxJQUFJLENBQUNFLFdBQUwsRUFBVjtBQUNFLFNBQU9ELEdBQUcsQ0FBQ0UsT0FBSixDQUFZLHlDQUFaLEVBQ1AsVUFBVUMsQ0FBVixFQUFhO0FBQ1gsV0FBT0EsQ0FBQyxDQUFDQyxXQUFGLEVBQVA7QUFDRCxHQUhNLENBQVA7QUFJSCIsImZpbGUiOiIuL3BhZ2VzL3BhZ2UuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHtDb21wb25lbnR9IGZyb20gJ3JlYWN0JztcblxuY29uc3QgY29tcG9uZW50cyA9IHtcbn1cblxuY29uc3QgUGFnZSA9IHByb3BzID0+IHtcblxuICBjb25zdCBsaXN0SXRlbXMgPSBwcm9wcy5kYWRvcy5tYXAoKHJvdykgPT5cbiAgICA8ZGl2IGtleT17cm93LmlkLnRvU3RyaW5nKCl9IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAge1xuICAgICAgICByb3cuZWxlbWVudHMubWFwKGNvbHVtbiA9PiB7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYga2V5PXtjb2x1bW4uaWR9IGNsYXNzTmFtZT17YGNvbC0ke2NvbHVtbi5zZXR0aW5ncy5fY29sdW1uX3NpemV9YH0+IFxuICAgICAgICAgICAgeyBcbiAgICAgICAgICAgICAgY29sdW1uLmVsZW1lbnRzLm1hcCh3aWRnZXQgPT4ge1xuICAgICAgICAgICAgICAgIGlmKHVjd29yZHMod2lkZ2V0LndpZGdldFR5cGUpID09PSAnSW1hZ2UnKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4oXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXt3aWRnZXQuc2V0dGluZ3MuaW1hZ2UudXJsfSBhbHQ9e3dpZGdldC5zZXR0aW5ncy5pbWFnZS5hbHR9IGtleT17d2lkZ2V0LmlkfSAvPiAgICAgXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgfWVsc2UgaWYodWN3b3Jkcyh3aWRnZXQud2lkZ2V0VHlwZSkgPT09ICdIZWFkaW5nJykge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgPGhlYWRpbmcga2V5PXt3aWRnZXQuaWR9IGNsYXNzTmFtZT1cIlwiPnt3aWRnZXQuc2V0dGluZ3MudGl0bGV9PC9oZWFkaW5nPlxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIClcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICA8L2Rpdj5cblxuICAgIFxuICApO1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgIHtsaXN0SXRlbXN9XG4gICAgPC8+XG4gIClcbn1cblxuXG5leHBvcnQgZGVmYXVsdCBQYWdlO1xuXG5leHBvcnQgY29uc3QgZ2V0U3RhdGljUHJvcHMgPSBhc3luYyAoKSA9PiB7XG5cbiAgbGV0IGRhdGE7XG4gIGF3YWl0IGZldGNoKCdodHRwOi8vbG9jYWxob3N0Lz9yZXN0X3JvdXRlPS93cC92Mi9wYWdlcy9wZy1jb250YWN0JylcbiAgICAudGhlbihmdW5jdGlvbiAocmVzKSB7XG4gICAgICByZXR1cm4gcmVzLmpzb24oKTtcbiAgICB9KS50aGVuKGZ1bmN0aW9uIChqc29uKSB7XG4gICAgICBkYXRhID0ganNvbjtcbiAgICAgIHJldHVybiBkYXRhO1xuICAgIH0pO1xuXG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIGRhZG9zOiBkYXRhICAvLyB3aWxsIGJlIHBhc3NlZCB0byB0aGUgcGFnZSBjb21wb25lbnQgYXMgcHJvcHNcbiAgICB9LFxuICB9XG59XG5cbmZ1bmN0aW9uIHVjd29yZHModGV4dCkge1xuICAgIFxuICBsZXQgc3RyID0gdGV4dC50b0xvd2VyQ2FzZSgpO1xuICAgIHJldHVybiBzdHIucmVwbGFjZSgvKF4oW2EtekEtWlxccHtNfV0pKXwoWyAtXVthLXpBLVpcXHB7TX1dKS9nLFxuICAgIGZ1bmN0aW9uIChzKSB7XG4gICAgICByZXR1cm4gcy50b1VwcGVyQ2FzZSgpO1xuICAgIH0pXG59XG5cblxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/page.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/page.js"));
module.exports = __webpack_exports__;

})();