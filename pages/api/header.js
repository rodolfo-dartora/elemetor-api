// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  fetchHeader()
  // console.log("teste",teste)
  // console.log(Header())
  res.status(200).json({ name: 'John Doe' })
}


export async function fetchHeader() {
  try {
    const res = await fetch(`http://localhost/elementor/theme/header`);
    console.log('res',res)
    const data = await res.json()
    
  
    if (!data) {
      return {
        notFound: true,
      }
    }
  
    return {
      props: { data }, // will be passed to the page component as props
    }
  } catch (error) {
    
  }
 
}