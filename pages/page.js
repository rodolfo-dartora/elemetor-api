import React, {Component} from 'react';


const Page = props => {

  const listItems = props.dados.map((row) =>
    <div key={row.id.toString()} className="row">
      {
        row.elements.map(column => {
          return (
            <div key={column.id} className={`col-${column.settings._column_size}`}> 
            { 
              column.elements.map(widget => {
                if(ucwords(widget.widgetType) === 'Image') {
                  return(
                    <img src={widget.settings.image.url} alt={widget.settings.image.alt} key={widget.id} />     
                  )
                }else if(ucwords(widget.widgetType) === 'Heading') {
                  return (
                    <heading key={widget.id} className="">{widget.settings.title}</heading>
                  )
                }
              })
            }
            </div>
          )
        })
      }
    </div>

    
  );
  return (
    <>
     {listItems}
    </>
  )
}


export default Page;

export const getStaticProps = async () => {

  let data;
  await fetch('http://localhost/?rest_route=/wp/v2/pages/pg-contact')
    .then(function (res) {
      return res.json();
    }).then(function (json) {
      data = json;
      return data;
    });

  return {
    props: {
      dados: data  // will be passed to the page component as props
    },
  }
}

function ucwords(text) {
    
  let str = text.toLowerCase();
    return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
    function (s) {
      return s.toUpperCase();
    })
}


